import React from "react";
import LoginNavbar from "./components/LoginNavbar";
import Footer from "./components/Footer";
import "./Login.css";
import eightlogo from "./assets/8logo.png";

function InputField(props) {
  return (
    <>
      <input
        className="Input-field-login"
        placeholder={props.placeholderText}
      />
    </>
  );
}

function Login() {
  return (
    <>
      <LoginNavbar pageTitle="Login" />

      <div className="Login-app">
        <header className="Login-app-header">
          <div class="grid-container">
            <div class="grid-item1">
              <img src={eightlogo} alt="logo" className="Eight-logo" />
            </div>
            <div class="grid-item2">
              <p className="sign-in">Sign In</p>
              <InputField placeholderText="Email" />
              <div />
              <InputField placeholderText="Password" />
              <div />

              <form action="/">
                <button className="Login-button" href="/">
                  Login
                </button>
              </form>

              <div />
              <a href="/dsds" style={{ fontSize: "12px" }}>
                Forgot account details
              </a>
            </div>
          </div>
        </header>
      </div>

      <Footer />
    </>
  );
}

export default Login;
