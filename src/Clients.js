import React from "react";
import NavigationBar from "./components/NavigationBar";
import Footer from "./components/Footer";
import "./Clients.css";
import { Table, Tabs, Tab } from "react-bootstrap";

function UserTable() {
  return (
    <div className="user-table4">
      <Table striped hover>
        <thead>
          <tr>
            <th>Client</th>
            <th>Main Contact</th>
            <th>Contact Number</th>
            <th>Active Assessments</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <a href="/ewqe">Evalue8 Sustainability</a>
            </td>
            <td></td>
            <td></td>
            <td>1</td>
          </tr>
          <tr>
            <td>
              <a href="/ewqe">Carbon Planet</a>
            </td>
            <td></td>
            <td></td>
            <td>2</td>
          </tr>
        </tbody>
      </Table>
    </div>
  );
}

function InputField(props) {
  return (
    <>
      <input className="Input-field4" placeholder={props.placeholderText} />
    </>
  );
}

function Clients() {
  return (
    <>
      <NavigationBar pageTitle="Clients" />

      <div className="App-clients">
        <header className="App-header-clients">
          <div className="Tab-container">
            <Tabs defaultActiveKey="allClients" className="Tab-head">
              <Tab eventKey="allClients" title="All Clients">
                <button className="Button-add2">+</button>
                <InputField placeholderText={"Search Clients"} />
                <UserTable />
              </Tab>
              <Tab eventKey="yourClients" title="Your Clients">
                <button className="Button-add2">+</button>
                <InputField placeholderText={"Search Clients"} />
                <UserTable />
              </Tab>
            </Tabs>
          </div>
        </header>
      </div>

      <Footer />
    </>
  );
}

export default Clients;
