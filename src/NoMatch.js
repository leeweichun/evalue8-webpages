import React from "react";
import Warning from "./assets/warningsign.png";
import Footer from "./components/Footer";
import NavigationBar from "./components/NavigationBar";

// This is the 404 page

function NoMatch() {
  return (
    <>
      <NavigationBar pageTitle="Page Not Found" />
      <div style={{ marginTop: "10vh", marginBottom: "10vh" }}>
        <img
          src={Warning}
          alt="warning-sign"
          style={{
            display: "block",
            marginLeft: "auto",
            marginRight: "auto",
            height: "20vh",
            marginBottom: "5vh",
          }}
        />
        <div
          style={{
            fontSize: "300%",
            textAlign: "center",
            fontWeight: "bold",
            color: "rgba(82, 58, 144, 1)",
          }}
        >
          404
        </div>
        <div
          style={{
            fontSize: "100%",
            textAlign: "center",
            fontWeight: "bold",
            color: "rgba(82, 58, 144, 1)",
          }}
        >
          The page you are looking for does not exist. Please let us know if
          this is an error.
        </div>
      </div>
      <Footer />
    </>
  );
}

export default NoMatch;
