import React from "react";
import NavigationBar from "./components/NavigationBar";
import Footer from "./components/Footer";
import "./SupportRequest.css";

function SupportRequest() {
  return (
    <>
      <NavigationBar pageTitle="Support Request" />

      <div className="App-supportrequest">
        <header className="App-header-supportrequest">
          <h1>YOUR CODE HERE</h1>
        </header>
      </div>

      <Footer />
    </>
  );
}

export default SupportRequest;
