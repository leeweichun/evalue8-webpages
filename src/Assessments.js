import React from "react";
import NavigationBar from "./components/NavigationBar";
import Footer from "./components/Footer";
import "./Assessments.css";
import { Table, Tabs, Tab } from "react-bootstrap";

function UserTable() {
  return (
    <div className="user-table3">
      <Table striped hover>
        <thead>
          <tr>
            <th>Label</th>
            <th>Client</th>
            <th>Period</th>
            <th>Status</th>
            <th>Due Date</th>
            <th>Lite</th>
            <th>Days Open</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <a href="/ewqe">Assessment 1</a>
            </td>
            <td>Evalue8 Sustainability</td>
            <td>FY2019</td>
            <td>Open</td>
            <td>2020-01-04</td>
            <td>-</td>
            <td>
              108<a href="/ewqe">(Overdue)</a>
            </td>
          </tr>
          <tr>
            <td>
              <a href="/ewqe">Assessment 2</a>
            </td>
            <td>Evalue8 Sustainability</td>
            <td>FY2020</td>
            <td>Open</td>
            <td>2020-08-02</td>
            <td>-</td>
            <td>23</td>
          </tr>
          <tr>
            <td>-</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>-</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
        </tbody>
      </Table>
    </div>
  );
}

function InputField(props) {
  return (
    <>
      <input className="Input-field4" placeholder={props.placeholderText} />
    </>
  );
}

function Assessments() {
  return (
    <>
      <NavigationBar pageTitle="Assessments" />

      <div className="App-assessments">
        <header className="App-header-assessments">
          <div className="Tab-container">
            <Tabs defaultActiveKey="allAssessments" className="Tab-head">
              <Tab eventKey="allAssessments" title="All Assessments">
                <button className="Button-add2">+</button>
                <InputField placeholderText={"Search Companies"} />
                <UserTable />
              </Tab>
              <Tab eventKey="yourAssessments" title="Your Assessments">
                <button className="Button-add2">+</button>
                <InputField placeholderText={"Search Companies"} />
                <UserTable />
              </Tab>
            </Tabs>
          </div>
        </header>
      </div>

      <Footer />
    </>
  );
}

export default Assessments;
