import React from "react";
import NavigationBar from "./components/NavigationBar";
import Footer from "./components/Footer";
import "./AssessmentCompanies.css";
import { Table } from "react-bootstrap";

function UserTable() {
  return (
    <div className="user-table2">
      <Table striped hover>
        <thead>
          <tr>
            <th>Company</th>
            <th>Type</th>
            <th>Parent</th>
            <th>Status</th>
            <th>Change Assessing Company</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Carbon Planet</td>
            <td>Master</td>
            <td></td>
            <td>Active</td>
            <td>
              <a href="/sdas">Select Here</a>
            </td>
          </tr>
          <tr>
            <td>Origin</td>
            <td>Affiliate</td>
            <td>Carbon Planet</td>
            <td>Active</td>
            <td>
              <a href="/sdas">Select Here</a>
            </td>
          </tr>
          <tr>
            <td>-</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>-</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
        </tbody>
      </Table>
    </div>
  );
}

function InputField(props) {
  return (
    <>
      <input className="Input-field3" placeholder={props.placeholderText} />
    </>
  );
}

function AssessmentCompanies() {
  return (
    <>
      <NavigationBar pageTitle="Assessment Companies" />

      <div className="App-assessmentCompanies">
        <header className="App-header-assessmentCompanies">
          <div>
            <button className="Button-add">+</button>
            <InputField placeholderText={"Search Companies"} />
            <UserTable />
          </div>
        </header>
      </div>

      <Footer />
    </>
  );
}

export default AssessmentCompanies;
