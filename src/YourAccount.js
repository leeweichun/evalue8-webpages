import React from "react";
import NavigationBar from "./components/NavigationBar";
import Footer from "./components/Footer";
import { Table, Tab, Row, Col, Nav } from "react-bootstrap";
import "./YourAccount.css";

function InputField(props) {
  return (
    <>
      <input className="Input-field" placeholder={props.placeholderText} />
    </>
  );
}

function Details() {
  return (
    <div className="details">
      <InputField placeholderText="First Name" />
      <InputField placeholderText="Last Name" />
      <InputField placeholderText="Email" />
      <InputField placeholderText="Login ID" />
      <select className="select-field" id="id" name="name">
        <option value="" disabled selected>
          Select Home Company
        </option>
        <option value="Evalue8">Evalue8</option>
        <option value="Carbon Planet">Carbon Planet</option>
      </select>
      Status: Active
    </div>
  );
}

function UserTable() {
  return (
    <div className="user-table">
      <Table striped hover>
        <thead>
          <tr>
            <th>Associated Assessments</th>
            <th>Roles</th>
            <th>Reporting Period</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <a href="/sdas">Assessment 1</a>
            </td>
            <td>Assessor, Commissioner....</td>
            <td>FY2019</td>
          </tr>
          <tr>
            <td>
              <a href="/sdas">Assessment 2</a>
            </td>
            <td>Assessor, Commissioner....</td>
            <td>FY2020</td>
          </tr>
          <tr>
            <td>
              <a href="/sdas">Assessment 3</a>
            </td>
            <td>Assessor, Commissioner....</td>
            <td>FY2018</td>
          </tr>
        </tbody>
      </Table>
    </div>
  );
}

function YourAccount() {
  return (
    <>
      <NavigationBar pageTitle="Your Account" />

      <div className="App">
        <header className="App-header">
          <Tab.Container
            id="left-tabs"
            defaultActiveKey="details"
            className="tab-item"
          >
            <Row>
              <Col sm={3}>
                <Nav variant="pills" className="flex-column">
                  <Nav.Item>
                    <Nav.Link eventKey="details">Details</Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link eventKey="systemRoles">System Roles</Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link eventKey="companyRoles">Company Roles</Nav.Link>
                  </Nav.Item>
                </Nav>
              </Col>
              <Col sm={9}>
                <Tab.Content>
                  <Tab.Pane
                    eventKey="details"
                    title="Details"
                    className="Tab-pane"
                  >
                    <Details />
                  </Tab.Pane>
                  <Tab.Pane
                    eventKey="systemRoles"
                    title="System Roles"
                    className="Tab-pane"
                  >
                    <h1>System Roles</h1>
                  </Tab.Pane>
                  <Tab.Pane
                    eventKey="companyRoles"
                    title="System Roles"
                    className="Tab-pane"
                  >
                    <h1>Company Roles</h1>
                  </Tab.Pane>
                </Tab.Content>
              </Col>
            </Row>
          </Tab.Container>

          <UserTable />
        </header>
      </div>

      <Footer />
    </>
  );
}

export default YourAccount;
