import React from "react";
import NavigationBar from "./components/NavigationBar";
import Footer from "./components/Footer";
import "./AuditTrail.css";
import { Table } from "react-bootstrap";

function UserTable() {
  return (
    <div className="user-table6">
      <Table striped hover>
        <thead>
          <tr>
            <th>Time</th>
            <th>User</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <tr>-</tr>
          <tr></tr>
          <tr></tr>
        </tbody>
      </Table>
    </div>
  );
}

function InputField(props) {
  return (
    <>
      <input className="Input-field6" placeholder={props.placeholderText} />
    </>
  );
}

function AuditTrail() {
  return (
    <>
      <NavigationBar pageTitle="Audit Trail" />

      <div className="App-auditTrail">
        <header className="App-header-auditTrail">
          <div>
            <InputField placeholderText={"Search"} />
            <UserTable />
          </div>
        </header>
      </div>

      <Footer />
    </>
  );
}

export default AuditTrail;
