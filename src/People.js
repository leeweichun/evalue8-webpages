import React from "react";
import NavigationBar from "./components/NavigationBar";
import Footer from "./components/Footer";
import "./People.css";
import { Table } from "react-bootstrap";

function UserTable() {
  return (
    <div className="user-table5">
      <Table striped hover>
        <thead>
          <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Status</th>
            <th>Home Company</th>
            <th>System Roles</th>
            <th>Roles in Current Company</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <a href="/sdas">Hashmim, Sherbaz</a>
            </td>
            <td>sherbaz@evalue8.net</td>
            <td>Activating</td>
            <td>Carbon Planet</td>
            <td>Configurator</td>
            <td>Staff</td>
          </tr>
          <tr>
            <td>
              <a href="/sdas">last name, first name</a>
            </td>
            <td>admintest@example.com</td>
            <td>Activated</td>
            <td>Carbon Planet</td>
            <td>Admin, Configurator</td>
            <td>-</td>
          </tr>
        </tbody>
      </Table>
    </div>
  );
}

function InputField(props) {
  return (
    <>
      <input className="Input-field5" placeholder={props.placeholderText} />
    </>
  );
}

function People() {
  return (
    <>
      <NavigationBar pageTitle="People" />

      <div className="App-people">
        <header className="App-header-people">
          <div>
            <button className="Button-add">+</button>
            <InputField placeholderText={"Search Companies"} />
            <UserTable />
          </div>
        </header>
      </div>

      <Footer />
    </>
  );
}

export default People;
