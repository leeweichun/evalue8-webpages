import React from "react";
import userImage from "../assets/userImage.png";
import logo_borderless from "../assets/logo-borderless.png";
import "./NavigationBar.css";

function NavigationBar(props) {
  return (
    <nav>
      <div className="Nav-bar">
        <ul className="Nav-list">
          <li className="Nav-list-item" style={{ marginRight: "30px" }}>
            <a href="/">
              <img
                src={logo_borderless}
                className="Navbar-logo"
                alt="logo"
                style={{ paddingTop: "2px" }}
              />
            </a>
          </li>
          <li className="Nav-list-item">
            <a href="/Clients" className="Nav-link">
              Clients
            </a>
          </li>
          <li className="Nav-list-item">
            <a href="/Assessments" className="Nav-link">
              Assessments
            </a>
          </li>
          <li className="Nav-list-item">
            <a href="/AssessmentCompanies" className="Nav-link">
              Assessment Companies
            </a>
          </li>
          <li className="Nav-list-item">
            <a href="/People" className="Nav-link">
              People
            </a>
          </li>

          <li className="Nav-list-item-right" style={{ float: "right" }}>
            <div className="Dropdown">
              <img src={userImage} className="User-image" alt="userImage" />
              <div className="Dropdown-content">
                <a href="/youraccount">Your Account</a>
                <div></div>
                <a href="/login">Logout</a>
              </div>
            </div>
          </li>
          <li className="Nav-list-item-right" style={{ float: "right" }}>
            <input className="Search-field" placeholder="Search..." />
          </li>
        </ul>
      </div>
      <div className="Title-bar">{props.pageTitle}</div>
    </nav>
  );
}

export default NavigationBar;
