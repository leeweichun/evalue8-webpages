import React, { Component } from "react";
import { Line } from "react-chartjs-2";
import Datafile from "../datafiles/energy-1.json";

// Used in Home page to demonstrate ability to read JSON file and output in a chart
const data = {
  labels: ["Subcircuit A", "Subcircuit B", "Subcircuit C"],
  datasets: [
    {
      label: "Subcircuit Energy",
      fill: true,
      lineTension: 0.3,
      backgroundColor: "rgba(0, 190, 163, .4)",
      borderCapStyle: "butt",
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: "miter",
      pointBorderColor: "rgba(82, 58, 144, 1)",
      pointBackgroundColor: "#fff",
      pointBorderWidth: 5,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: "rgba(75,192,192,1)",
      pointHoverBorderColor: "rgba(220,220,220,1)",
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: [
        Datafile["subcircuit"]["Subcircuit A"],
        Datafile["subcircuit"]["Subcircuit B"],
        Datafile["subcircuit"]["Subcircuit C"],
      ],
    },
  ],
};

export default class ChartDemo extends Component {
  render() {
    return (
      <div>
        <Line ref="chart" data={data} />
      </div>
    );
  }

  componentDidMount() {
    const { datasets } = this.refs.chart.chartInstance.data;
    console.log(datasets[0].data);
  }
}
