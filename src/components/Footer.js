import React from "react";
import "./Footer.css";

// Footer displayed on every page
function Footer() {
  return (
    <>
      <div className="Footer-line">
        Evalue8 Licenses and footer. Website prototype by Clarence Lee 2020.
      </div>
    </>
  );
}

export default Footer;
