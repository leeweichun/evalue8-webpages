import React from "react";
import logo_borderless from "../assets/logo-borderless.png";

// Navbar with just the logo for the login page
function LoginNavbar(props) {
  return (
    <nav>
      <img
        src={logo_borderless}
        className="Navbar-logo"
        alt="logo"
        style={{
          display: "block",
          paddingTop: "15px",
          paddingBottom: "15px",
          marginLeft: "auto",
          marginRight: "auto",
        }}
      />
    </nav>
  );
}

export default LoginNavbar;
