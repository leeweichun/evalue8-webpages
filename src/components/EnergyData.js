import React from "react";
import Datafile from "../datafiles/energy-1.json";
import { Table } from "react-bootstrap";

// Used in Home page to demonstrate ability to read JSON file and output in a table
function UserTable() {
  return (
    <div className="user-table3">
      <Table striped hover>
        <thead>
          <tr>
            <th>Start</th>
            <th>End</th>
            <th>Generated</th>
            <th>Consumed</th>
            <th>Sub A</th>
            <th>Sub B</th>
            <th>Sub C</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>{Datafile["timestamp"]["start"]}</td>
            <td>{Datafile["timestamp"]["end"]}</td>
            <td>{Datafile["energy-generated"]}</td>
            <td>{Datafile["energy-consumed"]}</td>
            <td>{Datafile["subcircuit"]["Subcircuit A"]}</td>
            <td>{Datafile["subcircuit"]["Subcircuit B"]}</td>
            <td>{Datafile["subcircuit"]["Subcircuit C"]}</td>
          </tr>
        </tbody>
      </Table>
    </div>
  );
}

function EnergyData() {
  return (
    <>
      <h3 style={{ marginTop: "50px", color: "lightgrey" }}>
        (Temp) Energy Data Display from JSON File
      </h3>
      <div>Site Name: {Datafile["site-name"]}</div>
      <UserTable />
    </>
  );
}

export default EnergyData;
