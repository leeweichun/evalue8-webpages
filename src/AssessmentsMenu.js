import React from "react";
import NavigationBar from "./components/NavigationBar";
import Footer from "./components/Footer";
import "./AssessmentsMenu.css";
import { Table, Tab, Row, Col, Nav, TabContainer } from "react-bootstrap";

function AssessmentsMenu() {
  return (
    <>
      <NavigationBar pageTitle="Assessments Menu" />
      <div className="App-assessmentsmenu">
        <header className="App-header-assessmentsmenu">
          <div className="h11"> Assessment: Assessment 1</div>
          <TabContainer defaultActiveKey="overview">
            <Row>
              <div className="content">
                <Col sm={2}>
                  <Nav variant="pills" className="flex-column">
                    <Nav.Item>
                      <Nav.Link eventKey="overview">Overview</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                      <Nav.Link eventKey="organization">Organization</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                      <Nav.Link eventKey="scope">Scope</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                      <Nav.Link eventKey="activities">Activities</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                      <Nav.Link eventKey="history">History</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                      <Nav.Link eventKey="reports">Reports</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                      <Nav.Link eventKey="comments">Comments</Nav.Link>
                    </Nav.Item>
                  </Nav>
                </Col>
              </div>
              <Col sm={8}>
                <Tab.Content>
                  <Tab.Pane eventKey="overview">
                    <div className="usertable">
                      <Table striped hover>
                        <thead>
                          <tr>
                            <th>Team</th>
                            <th>Progress</th>
                            <th>Dates</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>First Name</td>
                            <td>Assign Assessor</td>
                            <td>Assessment Created</td>
                          </tr>
                          <tr>
                            <td>.</td>
                            <td></td>
                            <td></td>
                          </tr>
                        </tbody>
                      </Table>
                    </div>
                  </Tab.Pane>
                  <Tab.Pane eventKey="organization">
                    <div className="org">
                      <div className="h22">Assessment Hierarchy</div>
                      <p>
                        On this screen you will find the hierarchy of entities
                        being assessed for their emissions. Often there will
                        only be one entity in an assessment, eg for a small
                        business. However. new entities can be added here, and
                        the organisational structure defined. When you click the
                        Add Entity button you’ll be asked to complete the
                        details of the entity, whether it be a branch office, a
                        physical site, or whatever. Then you’ll be asked to
                        complete the details of the entity, wether a branch
                        office, a physical site, or whatever. Then you’ll come
                        back here and you can then debate the newly created
                        entity into the correct spot in the hierarchy.
                      </p>
                    </div>
                  </Tab.Pane>
                  <Tab.Pane eventKey="scope">
                    <div className="usertable">
                      <Table striped hover>
                        <thead>
                          <tr>
                            <th>Entity</th>
                            <th>Status</th>
                            <th>Activities</th>
                            <th>Export</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Evalue8 Sustainability</td>
                            <td>Reviewed</td>
                            <td>
                              <a href="/ewqe">1 Activity</a>
                            </td>
                            <td>
                              <a href="/ewqe">Spreadsheet</a>
                            </td>
                          </tr>
                          <tr>
                            <td>.</td>
                            <td></td>
                            <td></td>
                            <td></td>
                          </tr>
                        </tbody>
                      </Table>
                    </div>
                    <div className="pp">
                      {" "}
                      Scope means the set of activites being measured for each
                      entity. Here you will find a list of all entities involved
                      in this assessment (often only one) and which activities
                      have been assigned to each entity.
                    </div>
                  </Tab.Pane>
                  <Tab.Pane eventKey="activities">
                    <div className="usertable">
                      <Table striped hover>
                        <thead>
                          <tr>
                            <th>Activity</th>
                            <th>Data Collector</th>
                            <th>Last Updated</th>
                            <th>Data Collection Due</th>
                            <th>Status</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Scope 1</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                          </tr>
                          <tr>
                            <td>
                              <a href="/ewqe">Electricity</a>
                            </td>
                            <td>Name</td>
                            <td>2019-12-11</td>
                            <td>2019-12-10</td>
                            <td>
                              <a href="/ewqe">Reviewed</a>
                            </td>
                          </tr>
                          <tr>
                            <td>.</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                          </tr>
                        </tbody>
                      </Table>
                    </div>
                  </Tab.Pane>
                  <Tab.Pane eventKey="history">
                    <div className="usertable">
                      <Table striped hover>
                        <thead>
                          <tr>
                            <th>Time</th>
                            <th>User</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>.</td>
                            <td></td>
                            <td></td>
                          </tr>
                          <tr>
                            <td>.</td>
                            <td></td>
                            <td></td>
                          </tr>
                        </tbody>
                      </Table>
                    </div>
                  </Tab.Pane>
                  <Tab.Pane eventKey="reports">
                    "#Assessment id:6, name: “Assessment 1”, default_due_date:
                    “2020-01-04” created_by_id:9"
                  </Tab.Pane>
                  <Tab.Pane eventKey="comments">
                    <div className="h33">
                      This section is available to carbon planet senior
                      assessers and staff only.
                    </div>
                  </Tab.Pane>
                </Tab.Content>
              </Col>
            </Row>
          </TabContainer>
        </header>
      </div>

      <Footer />
    </>
  );
}
export default AssessmentsMenu;
