import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from "./Home";
import Clients from "./Clients";
import Assessments from "./Assessments";
import AssessmentCompanies from "./AssessmentCompanies";
import NoMatch from "./NoMatch";
import People from "./People";
import YourAccount from "./YourAccount";
import Login from "./Login";
import AssessmentsMenu from "./AssessmentsMenu";
import AuditTrail from "./AuditTrail";
import SupportRequest from "./SupportRequest";

function App() {
  return (
    <>
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/clients" component={Clients} />
          <Route path="/assessments" component={Assessments} />
          <Route path="/assessmentcompanies" component={AssessmentCompanies} />
          <Route path="/people" component={People} />
          <Route path="/youraccount" component={YourAccount} />
          <Route path="/login" component={Login} />
          <Route path="/supportrequest" component={SupportRequest} />
          <Route path="/audittrail" component={AuditTrail} />
          <Route path="/assessmentsmenu" component={AssessmentsMenu} />
          <Route component={NoMatch} />
        </Switch>
      </Router>
    </>
  );
}

export default App;
