import React from "react";
import NavigationBar from "./components/NavigationBar";
import Footer from "./components/Footer";
import EnergyData from "./components/EnergyData";
import ChartDemo from "./components/ChartDemo";
import "./Home.css";

function Home() {
  return (
    <>
      <NavigationBar pageTitle="Home" />

      <div className="App">
        <div class="grid-container2">
          <div class="grid2-item1">
            <p className="section-title">Recent Updates</p>
            <EnergyData />
          </div>
          <div class="grid2-item2">
            <p className="section-title">Current Assessment</p>
            <ChartDemo />
          </div>
          <div class="grid2-item3">
            <p className="section-title">To Do List</p>
            <p>Hidden pages:</p>
            <a href="/AuditTrail">Audit Trail</a>
            <div></div>
            <a href="/AssessmentsMenu">Assessments Menu</a>
            <div></div>
            <a href="/SupportRequest">Support Request</a>
          </div>
        </div>
      </div>

      <Footer />
    </>
  );
}

export default Home;
